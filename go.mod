module gitlab.com/vsenko/tinystash

go 1.13

require (
	github.com/didip/tollbooth v4.0.2+incompatible
	github.com/patrickmn/go-cache v0.0.0-20170418232947-7ac151875ffb
	golang.org/x/crypto v0.0.0-20191122220453-ac88ee75c92c
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
)
