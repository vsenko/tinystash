package main

import (
	"math/rand"
	"time"

	"gitlab.com/vsenko/tinystash/service"
)

func main() {
	rand.Seed(time.Now().Unix())
	service.Start()
}
