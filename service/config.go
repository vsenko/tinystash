package service

import "time"

type ServiceConfig struct {
	ListenDomains        []string
	LimiterMaxPerSecond  float64
	LimiterExpirationTTL time.Duration
	CacheExpiration      time.Duration
	CacheCleanupInterval time.Duration
	MaxCacheEntries      int
	MaxKeyBytes          int
	MaxValueBytes        int64
}
