package service

import "time"

type cacheEntry struct {
	address   string
	value     string
	timestamp time.Time
}
