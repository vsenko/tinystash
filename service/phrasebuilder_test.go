package service

import (
	"math/rand"
	"testing"
)

func TestGetPhrase(t *testing.T) {
	rand.Seed(1)
	phrase := getPhrase()
	expectedPhrase := "express peaceful night"
	if phrase != expectedPhrase {
		t.Errorf("Unexpected phrase: \"%v\", expected\"%v\"", phrase, expectedPhrase)
	}
}
