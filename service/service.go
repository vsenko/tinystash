package service

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/didip/tollbooth"
	"github.com/didip/tollbooth/limiter"
	"github.com/patrickmn/go-cache"
	"golang.org/x/crypto/acme/autocert"
)

var serviceConfig ServiceConfig = ServiceConfig{
	ListenDomains:        []string{},
	LimiterMaxPerSecond:  5,
	LimiterExpirationTTL: time.Minute,
	CacheExpiration:      24 * time.Hour,
	CacheCleanupInterval: 10 * time.Minute,
	MaxCacheEntries:      1000,
	MaxKeyBytes:          128,
	MaxValueBytes:        1024,
}

var c *cache.Cache = cache.New(serviceConfig.CacheExpiration, serviceConfig.CacheCleanupInterval)

var defaultTemplate *template.Template = parseTemplate(defaultTemplateText)

func parseTemplate(templateText string) *template.Template {
	t, err := template.New("webpage").Parse(templateText)
	if err != nil {
		if err != nil {
			log.Fatal(err)
		}
	}

	return t
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	must := func(err error) {
		if err != nil {
			log.Fatal(err)
		}
	}

	switch r.Method {
	case "GET":
		cacheKey := ""

		if key, ok := r.URL.Query()["key"]; ok {
			cacheKey = key[0]
		} else {
			must(defaultTemplate.Execute(w, defaultTemplateData{}))
			return
		}

		if len(cacheKey) > serviceConfig.MaxKeyBytes {
			must(defaultTemplate.Execute(w, defaultTemplateData{
				Error: fmt.Sprintf("\"key\" is too long")}))
			return
		}

		value, found := c.Get(cacheKey)
		if !found {
			w.WriteHeader(http.StatusNotFound)
			must(defaultTemplate.Execute(w, defaultTemplateData{
				Error: fmt.Sprintf("\"key\" `%v` not found", cacheKey)}))
			return
		}
		entry, typeOk := value.(*cacheEntry)
		if !typeOk {
			log.Fatalf("[rootHandler]: failed to extract cache entry for \"key\" `%v`: type conversion failed\n", cacheKey)
		}

		c.Delete(cacheKey)

		must(defaultTemplate.Execute(w, defaultTemplateData{
			Key:       cacheKey,
			Value:     entry.value,
			Address:   entry.address,
			Timestamp: entry.timestamp.String()}))
	case "POST":
		r.Body = http.MaxBytesReader(w, r.Body, serviceConfig.MaxValueBytes)
		err := r.ParseForm()
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			must(defaultTemplate.Execute(w, defaultTemplateData{
				Error: "Could not parse form"}))
			return
		}

		cacheKey := ""
		if key, ok := r.Form["key"]; ok {
			cacheKey = key[0]
		}

		if len(cacheKey) > serviceConfig.MaxKeyBytes {
			must(defaultTemplate.Execute(w, defaultTemplateData{
				Error: fmt.Sprintf("\"key\" is too long")}))
			return
		}

		if cacheKey == "" {
			cacheKey = getPhrase()
			_, contains := c.Get(cacheKey)
			for contains {
				cacheKey = getPhrase()
				_, contains = c.Get(cacheKey)
			}
		}

		cacheValue := ""
		if value, ok := r.Form["value"]; ok {
			cacheValue = value[0]
		}
		if cacheValue == "" {
			w.WriteHeader(http.StatusBadRequest)
			must(defaultTemplate.Execute(w, defaultTemplateData{
				Error: "Request contains no \"value\""}))
			return
		}

		if c.ItemCount() > serviceConfig.MaxCacheEntries {
			w.WriteHeader(http.StatusBadRequest)
			must(defaultTemplate.Execute(w, defaultTemplateData{
				Error: "Stash is full"}))
			return
		}

		entry := cacheEntry{
			address:   r.RemoteAddr,
			timestamp: time.Now(),
			value:     cacheValue,
		}
		c.Set(cacheKey, &entry, cache.DefaultExpiration)

		must(defaultTemplate.Execute(w, defaultTemplateData{
			Key:       cacheKey,
			Value:     cacheValue,
			Address:   entry.address,
			Timestamp: entry.timestamp.String()}))
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		must(defaultTemplate.Execute(w, defaultTemplateData{
			Error: "Method not supported"}))
	}
}

func statusHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Stored items: %v", c.ItemCount())
}

func apiHandler(w http.ResponseWriter, r *http.Request) {
	cacheKey := ""

	pathParts := strings.Split(r.URL.Path, "/")
	if len(pathParts) > 2 {
		cacheKey = pathParts[2]
	}

	if key, ok := r.URL.Query()["key"]; ok {
		cacheKey = key[0]
	}

	if len(cacheKey) > serviceConfig.MaxKeyBytes {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "\"key\" is too long `%v`", len(cacheKey))
		return
	}

	switch r.Method {
	case "GET":
		if cacheKey == "" {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "Could not figure out \"key\"")
			return
		}

		value, found := c.Get(cacheKey)
		if !found {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, "\"key\" `%v` not found", cacheKey)
			return
		}
		entry, typeOk := value.(*cacheEntry)
		if !typeOk {
			log.Fatalf("[apiHandler]: failed to extract cache entry for \"key\" `%v`: type conversion failed\n", cacheKey)
		}

		c.Delete(cacheKey)

		fmt.Fprint(w, entry.value)
	case "POST":
		if cacheKey == "" {
			cacheKey = getPhrase()
			_, contains := c.Get(cacheKey)
			for contains {
				cacheKey = getPhrase()
				_, contains = c.Get(cacheKey)
			}
		}

		r.Body = http.MaxBytesReader(w, r.Body, serviceConfig.MaxValueBytes)

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "Could not read request `%v`", err)
			return
		}

		if c.ItemCount() > serviceConfig.MaxCacheEntries {
			w.WriteHeader(http.StatusInsufficientStorage)
			fmt.Fprint(w, "Stash is full")
			return
		}

		entry := cacheEntry{
			address:   r.RemoteAddr,
			timestamp: time.Now(),
			value:     string(body),
		}
		c.Set(cacheKey, &entry, cache.DefaultExpiration)

		fmt.Fprint(w, cacheKey)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprint(w, "Method not supported")
	}
}

func buildHandler() http.Handler {
	srvmux := http.NewServeMux()
	srvmux.HandleFunc("/", rootHandler)
	srvmux.HandleFunc("/status", statusHandler)
	srvmux.HandleFunc("/api/", apiHandler)
	srvmux.HandleFunc("/api", apiHandler)

	return srvmux
}

func buildLimitedHandler() http.Handler {
	lmt := tollbooth.NewLimiter(serviceConfig.LimiterMaxPerSecond, &limiter.ExpirableOptions{})
	return tollbooth.LimitHandler(lmt, buildHandler())
}

// Start service
func Start() {
	m := &autocert.Manager{
		Prompt: autocert.AcceptTOS,
		Cache:  autocert.DirCache(filepath.Join(os.Getenv("HOME"), ".cache", "tinystash-autocert")),
	}

	if len(serviceConfig.ListenDomains) > 0 {
		m.HostPolicy = autocert.HostWhitelist(serviceConfig.ListenDomains...)
	}

	log.Printf("Starting\n")

	go func() {
		s := &http.Server{
			Addr:    ":80",
			Handler: m.HTTPHandler(nil),
		}
		log.Fatal(s.ListenAndServe())
	}()

	log.Fatal(http.Serve(m.Listener(), buildLimitedHandler()))
}
