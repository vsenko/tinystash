package service

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"
)

var handler http.Handler = buildHandler()
var limitedHandler http.Handler = buildLimitedHandler()

func checkGoodResponse(req *http.Request, handler http.Handler, res string, t *testing.T) {
	w := httptest.NewRecorder()
	handler.ServeHTTP(w, req)

	resp := w.Result()

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code: \"%v\"", resp.StatusCode)
	}

	if res == "" {
		return
	}

	body, _ := ioutil.ReadAll(resp.Body)
	bodyString := string(body)
	if bodyString != res {
		t.Errorf("Unexpected response: \"%v\", expected: \"%v\"", bodyString, res)
	}
}

func checkBadResponse(req *http.Request, handler http.Handler, statusCode int, t *testing.T) {
	w := httptest.NewRecorder()
	handler.ServeHTTP(w, req)

	resp := w.Result()

	if resp.StatusCode != statusCode {
		t.Errorf("Unexpected status code: \"%v\", expected: \"%v\"", resp.StatusCode, statusCode)
	}
}

func getGoodResponse(req *http.Request, handler http.Handler, t *testing.T) string {
	w := httptest.NewRecorder()
	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	bodyString := string(body)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code: \"%v\"", resp.StatusCode)
	}

	return bodyString
}

func TestRootHandler(t *testing.T) {
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/", nil), handler, "", t)
}

func TestStatusHandler(t *testing.T) {
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 0", t)
}

func TestLimiter(t *testing.T) {
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), limitedHandler, "Stored items: 0", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), limitedHandler, "Stored items: 0", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), limitedHandler, "Stored items: 0", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), limitedHandler, "Stored items: 0", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), limitedHandler, "Stored items: 0", t)
	checkBadResponse(httptest.NewRequest("GET", "http://example.com/status", nil), limitedHandler, 429, t)
	checkBadResponse(httptest.NewRequest("GET", "http://example.com/status", nil), limitedHandler, 429, t)

	time.Sleep(1 * time.Second)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), limitedHandler, "Stored items: 0", t)
}

func TestApiHandlerSimpleByPath(t *testing.T) {
	data := "This is a test"

	c.Flush()
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 0", t)
	checkGoodResponse(httptest.NewRequest("POST", "http://example.com/api/xxxyyy", strings.NewReader(data)), handler, "xxxyyy", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 1", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/api/xxxyyy", nil), handler, data, t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 0", t)
}

func TestApiHandlerSimpleByQuery(t *testing.T) {
	data := "This is a testttttt"

	c.Flush()
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 0", t)
	checkGoodResponse(httptest.NewRequest("POST", "http://example.com/api?key=xxxyyy", strings.NewReader(data)), handler, "xxxyyy", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 1", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/api?key=xxxyyy", nil), handler, data, t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 0", t)
}

func TestApiHandlerSimpleByMixed(t *testing.T) {
	data := "This is a tessssssssssssssssssssssssssssssssssssssssssssssssssssssst"

	c.Flush()
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 0", t)
	checkGoodResponse(httptest.NewRequest("POST", "http://example.com/api?key=xxxyyy", strings.NewReader(data)), handler, "xxxyyy", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 1", t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/api/xxxyyy", nil), handler, data, t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 0", t)
}

func TestApiHandlerNoKey(t *testing.T) {
	data := "This is a test!"

	c.Flush()
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 0", t)
	key := getGoodResponse(httptest.NewRequest("POST", "http://example.com/api", strings.NewReader(data)), handler, t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 1", t)
	url := fmt.Sprintf("http://example.com/api/%v", url.PathEscape(key))
	checkGoodResponse(httptest.NewRequest("GET", url, nil), handler, data, t)
	checkGoodResponse(httptest.NewRequest("GET", "http://example.com/status", nil), handler, "Stored items: 0", t)
}

func TestApiHandlerTooMuchData(t *testing.T) {
	chars := make([]rune, 1025)
	for i := range chars {
		chars[i] = 'a'
	}
	data := string(chars)

	c.Flush()
	checkBadResponse(httptest.NewRequest("POST", "http://example.com/api/xxxyyy", strings.NewReader(data)), handler, 400, t)
}
