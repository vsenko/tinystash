package service

const defaultTemplateText = `
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>tiny stash</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">

			<div class="row">
				<div class="col">
					<h1>tiny stash</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-xl-5">
					<form action="/" method="get">
						<div class="input-group mb-3">
							<input type="text" name="key" class="form-control" placeholder="key" aria-label="key" aria-describedby="button-addon2">
							<div class="input-group-append">
								<button class="btn btn-outline-primary" type="submit" id="button-addon2">Query</button>
							</div>
						</div>
					</form>
				</div>

				<div class="col-xl-7">
					<form action="/" method="post">
						<div class="input-group mb-3">
							<input type="text" name="value" class="form-control" placeholder="value" aria-label="value" aria-describedby="button-addon2">
							<input type="text" name="key" class="form-control" placeholder="key (optional)" aria-label="key" aria-describedby="button-addon2">
							<div class="input-group-append">
								<button class="btn btn-outline-primary" type="submit" id="button-addon2">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>

			<hr>

			{{if .Error}}
			<div class="row">
				<div class="col">
					<p>Server encountered an error:<p>
					<p class="lead">{{.Error}}</p>
				</div>
			</div>

			{{else if .Key}}
			<div class="row">
				<div class="col">
					<p>The value for your key</p>
					<p class="lead">{{.Key}}</p>
					<p>is</p>
					<p class="lead">{{.Value}}</p>
					<p>submitted <strong>{{.Timestamp}}</strong> from <strong>{{.Address}}</strong></p>
				</div>
			</div>
			{{end}}

		</div>
	</body>
</html>
`

type defaultTemplateData struct {
	Error     string
	Key       string
	Value     string
	Address   string
	Timestamp string
}
